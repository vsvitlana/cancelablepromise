class CancelablePromise extends Promise {
  constructor(executor) {
    if (typeof executor !== 'function') {
      throw new Error('Executor must be a function');
    }
  
    let cancel;
    let chainedPromises = [];
    const cancelableExecutor = (resolve, reject) => {
      cancel = () => {
        this.isCanceled = true;
        reject({ isCanceled: true })

        for (const promise of chainedPromises) {
          promise.isCanceled = true;
          reject({ isCanceled: true })
        }
          
      };
      executor(resolve, reject, cancel);
    };
  
    super(cancelableExecutor);
    this.isCanceled = false;
    this.cancel = cancel;
    this.chainedPromises = chainedPromises;
  }
  
  then(onFulfilled, onRejected) {
    if ((onFulfilled && typeof onFulfilled !== "function")) {
      throw new Error("onFulfilled must be functions or omitted");
    }
  
    const p = super.then(onFulfilled, onRejected);
    p.cancel = this.cancel;
    this.chainedPromises.push(p);
    return p;
  }
}
  

module.exports = CancelablePromise